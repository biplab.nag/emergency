package com.app.android.emergencyhelp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.emergencyhelp.R;
import com.app.android.emergencyhelp.activity.ContactActivity;
import com.app.android.emergencyhelp.database.Contact;
import com.app.android.emergencyhelp.database.DatabaseSource;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.DataViewHolder>{

    private Context context;
    private List<Contact> contactList;

    public ContactAdapter(Context context, List<Contact> contactList) {
        this.context = context;
        this.contactList = contactList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v= inflater.inflate(R.layout.card_row,parent,false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, final int position) {
        holder.nameTV.setText(contactList.get(position).getName().toString());
        holder.phoneTV.setText(contactList.get(position).getPhone().toString());

        holder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseSource source = new DatabaseSource(context);
                 Boolean delete = source.deleteContact(contactList.get(position));

                 //-- check delete successful or not  --//
                if (delete){
                    Toast.makeText(context, "Successfully delete", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, ContactActivity.class);
                    context.startActivity(intent);
                }
                else {
                    Toast.makeText(context, "Can not delete this time. Please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder{

        TextView nameTV;
        TextView phoneTV;
        ImageButton delete_btn;

        public DataViewHolder(View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.name);
            phoneTV = itemView.findViewById(R.id.phone);
            delete_btn = itemView.findViewById(R.id.delete);

        }
    }
}
