package com.app.android.emergencyhelp.activity;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.emergencyhelp.R;
import com.app.android.emergencyhelp.adapter.ContactAdapter;
import com.app.android.emergencyhelp.database.Contact;
import com.app.android.emergencyhelp.database.DatabaseSource;

import java.util.List;

public class ContactActivity extends AppCompatActivity {


    private RecyclerView contactRV;
    private List<Contact> contactList;
    private ContactAdapter adapter;


    private EditText nameET;
    private EditText phoneET;
    private Button save_btn;

    private String name;
    private String phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //-- set action bar title --//
        getSupportActionBar().setTitle("Phone Number");

        //-- hide auto open keyboard--//
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //-- find view by id--//
        contactRV = findViewById(R.id.contactRV);
        nameET = findViewById(R.id.nameET);
        phoneET = findViewById(R.id.phoneET);
        save_btn = findViewById(R.id.save);

        //--add new Contact --//
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = nameET.getText().toString();
                phone = phoneET.getText().toString();

                //-- check if input field is empty or not --//
                if (TextUtils.isEmpty(name)) {
                    nameET.setError("Contact name cannot be empty");
                }
                else if (TextUtils.isEmpty(phone)) {
                    phoneET.setError("Phone number cannot be empty");
                }
                else if(phone.length() < 11){
                    phoneET.setError("Please enter a valid number");
                }
                else{
                    addContact();
                }
            }
        });


        //--load contact list --//
        loadContacts();


    }

    private void addContact() {
        //--add a new contact in database --//
        DatabaseSource source = new DatabaseSource(ContactActivity.this);
        Contact contact= new Contact(0,name,phone);
        Boolean add = source.addContact(contact);

        //-- check if add successfully or not --//
        if(add){
            nameET.setText("");
            phoneET.setText("");
            Toast.makeText(this, "Successfully added a new contact", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ContactActivity.this, ContactActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadContacts() {
        //--get all contact form database --//
        DatabaseSource source = new DatabaseSource(ContactActivity.this);
        contactList = source.getAllContact();
        int size = contactList.size();
        //-- check if the list is empty or not --//
        if(size != 0){
            adapter = new ContactAdapter(ContactActivity.this,contactList);
            LinearLayoutManager llm = new LinearLayoutManager(ContactActivity.this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            contactRV.setLayoutManager(llm);
            contactRV.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        else{
            Toast.makeText(this, "Please Add some emergency contacts", Toast.LENGTH_LONG).show();
        }
    }


    //--Menu back press go to main activity --//
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                startActivity(new Intent(ContactActivity.this,MainActivity.class));
                finish(); //close the activty
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
