package com.app.android.emergencyhelp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class DatabaseSource {

    private DatabaseHelper helper ;
    private SQLiteDatabase database ;

    //---constructor ---//
    public DatabaseSource(Context context ) {
        helper = new DatabaseHelper(context) ;
    }

    //---database opening method---//
    public void open(){
        database = helper.getWritableDatabase();
    }

    //---database closing method---//
    public void close(){
        database.close();
    }

    //---add contact method---//
    public Boolean addContact( Contact contact){
        this.open();
        ContentValues values = new ContentValues();
        values.put(helper.COL_NAME,contact.getName());
        values.put(helper.COL_PHONE,contact.getPhone());
        long inserted = database.insert(helper.TABLE_CONTACT,null,values);
        this.close();

        if(inserted >0){
            return true;
        }
        else{
            return false;
        }

    }

    //--- get all contact info data ---//
    public ArrayList<Contact> getAllContact(){
        this.open();
        ArrayList<Contact> list  = new ArrayList<>();
        Cursor cursor = database.query(helper.TABLE_CONTACT,null,null,null,null,null,null);

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(cursor.getColumnIndex(helper.COL_ID_CONTACT));
                String name = cursor.getString(cursor.getColumnIndex(helper.COL_NAME));
                String phone = cursor.getString(cursor.getColumnIndex(helper.COL_PHONE));

                Contact response = new Contact(id,name,phone);
                list.add(response);

            }while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return list;

    }

    //-- delete contact--//
    public Boolean deleteContact( Contact contact){
        this.open();
        int id = contact.getId();
        long inserted = database.delete(helper.TABLE_CONTACT,helper.COL_ID_CONTACT+"=?",new String[] {String.valueOf(id)});
        this.close();

        if(inserted >0){
            return true;
        }
        else{
            return false;
        }

    }


    //-- update location table --//
    public Boolean updateLocation(MyLocation location ){
        this.open();
        ContentValues values = new ContentValues();
        values.put(helper.COL_LAT,location.getLatitide());
        values.put(helper.COL_LONG,location.getLongitude());
        values.put(helper.COL_ADDRESS,location.getAddress());
        values.put(helper.COL_UPDATE_TIME,location.getUpdate_time());

        long inserted = database.update(helper.TABLE_LOCATION,values,null,null);
        this.close();
        if(inserted >0){
            return true;
        }
        else{
            return false;
        }


    }

    //-- update location table --//
    public ArrayList<MyLocation> getLocation(){
        this.open();
        ArrayList<MyLocation> list  = new ArrayList<>();
        Cursor cursor = database.query(helper.TABLE_LOCATION,null,null,null,null,null,null);

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            do {

                int id = cursor.getInt(cursor.getColumnIndex(helper.COL_ID_LOCATION));
                String latitide= cursor.getString(cursor.getColumnIndex(helper.COL_LAT));
                String longitude = cursor.getString(cursor.getColumnIndex(helper.COL_LONG));
                String address =  cursor.getString(cursor.getColumnIndex(helper.COL_ADDRESS));
                String update_time =  cursor.getString(cursor.getColumnIndex(helper.COL_UPDATE_TIME));

                MyLocation response = new MyLocation(id,latitide,longitude,address,update_time);
                list.add(response);

            }while (cursor.moveToNext());
        }
        cursor.close();
        this.close();

        return list;
    }
}
