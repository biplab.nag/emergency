package com.app.android.emergencyhelp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.emergencyhelp.R;
import com.app.android.emergencyhelp.database.Contact;
import com.app.android.emergencyhelp.database.DatabaseSource;
import com.app.android.emergencyhelp.database.MyLocation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {


    private Button loacation_btn;
    private Button phone_btn;
    private ImageButton sms_btn;

    private TextView latTV;
    private TextView longtTV;
    private TextView addressTV;
    private TextView timeTV;


    private FusedLocationProviderClient client;
    public static Location lastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //--find view by id --//
        loacation_btn = findViewById(R.id.locationbtn);
        phone_btn = findViewById(R.id.phonebtn);
        sms_btn = findViewById(R.id.smsbtn);

        latTV = findViewById(R.id.lat);
        longtTV = findViewById(R.id.longi);
        addressTV = findViewById(R.id.address);
        timeTV = findViewById(R.id.updatetime);

        //-- set location value--//
        setLocationValue();


        //--get current device location --//
        loacation_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    getLocation();
            }
        });

        //-- goto contact activity--//
        phone_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ContactActivity.class);
                startActivity(intent);
            }
        });

        //-- send emergency sms--//
        sms_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    sendSMS();

            }
        });
    }

    private void setLocationValue() {

        //--load data --//
        DatabaseSource source = new DatabaseSource(MainActivity.this);
        ArrayList<MyLocation> data = source.getLocation();

        latTV.setText("Latitude          :   "+data.get(0).getLatitide());
        longtTV.setText("Longitude       :   "+data.get(0).getLongitude());
        addressTV.setText("Address          :   \n"+data.get(0).getAddress());
        timeTV.setText("Last Updated :   \n"+data.get(0).getUpdate_time());
    }

    private void getLocation() {
        client = LocationServices.getFusedLocationProviderClient(MainActivity.this);

        //---check permission ---//
        checkPermission();

        client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {

                if (task.isSuccessful() && task != null) {
                    lastLocation = task.getResult();
                    String t = currentTime();
                    String a = getAddress(lastLocation.getLatitude(), lastLocation.getLongitude());

                    MyLocation myLocation = new MyLocation(0,String.valueOf(lastLocation.getLatitude()), String.valueOf(lastLocation.getLongitude()),a,t);

                    //-- update database  --//
                    DatabaseSource source = new DatabaseSource(MainActivity.this);
                    Boolean update = source.updateLocation(myLocation);

                    //--check --//
                    if(update){
                        setLocationValue();
                        Toast.makeText(MainActivity.this, "Get Current location information", Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Toast.makeText(MainActivity.this, "Please Try again", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void sendSMS() {
        //--check permission --//
        checkPermission();
        //-- get phone numbers--//
        DatabaseSource source = new DatabaseSource(MainActivity.this);
        List<Contact> contacts = source.getAllContact();
        int size = contacts.size();

        //-- load lat long --//
        ArrayList<MyLocation> locations = source.getLocation();
        String latitude = locations.get(0).getLatitide();
        String longitude = locations.get(0).getLongitude();
        String address = locations.get(0).getAddress();
        String message = "This is an Emergency, I'm in trouble and need help.\nMy current location :\n"+address+
                "\n https://maps.google.com/?q="+latitude+","+longitude+"&ll="+latitude+","+longitude+"&z=16";
        //-- check if the contact is empty or not --//
        if(size != 0){

            //-- send sms to all contact -//
            for(int i = 0 ; i <size ;i++){
                String phoneNumber = contacts.get(i).getPhone();
                String name = contacts.get(i).getName();
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNumber, null, message, null, null);
                    Toast.makeText(this, "Successfully Send Message to "+name, Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
                            Toast.LENGTH_LONG).show();
                    ex.printStackTrace();
                }
            }


        }
        else{
            Toast.makeText(this, "No Contacts available", Toast.LENGTH_SHORT).show();
        }


    }

    private String getAddress(double latitude , double longitude){

        String  ads =" ";
         String city =" ";
        Geocoder geocoder;
        List<Address> addressList ;
        geocoder= new Geocoder(MainActivity.this, Locale.getDefault());
        try {
            addressList = geocoder.getFromLocation(latitude,longitude,1);
            ads = addressList.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ads;
    }

    private String currentTime (){
        Date currentTime = Calendar.getInstance().getTime();

        return String.valueOf(currentTime);
    }

    private void checkPermission(){
        //--check permission --//
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS},21);
            return;
        }
    }


}
