package com.app.android.emergencyhelp.database;

public class MyLocation {
    private int  id;
    private String  latitide;
    private String  longitude;
    private String  address;
    private String  update_time;

    public MyLocation(int id, String latitide, String longitude, String address, String update_time) {
        this.id = id;
        this.latitide = latitide;
        this.longitude = longitude;
        this.address = address;
        this.update_time = update_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatitide() {
        return latitide;
    }

    public void setLatitide(String latitide) {
        this.latitide = latitide;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }
}
