package com.app.android.emergencyhelp.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DatabaseHelper extends SQLiteAssetHelper {

    //---database info---//
    public static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME = "appdata.db";

    //--- database table info---//
    public static final String TABLE_LOCATION ="location";
    public static final String TABLE_CONTACT ="contact";


    //--- contact table column info---//
    public static final String COL_ID_CONTACT ="id";
    public static final String COL_NAME ="name";
    public static final String COL_PHONE ="phone";


    //--- location table column info---//
    public static final String COL_ID_LOCATION ="id";
    public static final String COL_LAT ="lat";
    public static final String COL_LONG ="long";
    public static final String COL_ADDRESS ="address";
    public static final String COL_UPDATE_TIME ="update_time";


    //---Constractor for this class---//
    public DatabaseHelper(Context context ) {
        super(context,DATABASE_NAME ,null, DATABASE_VERSION);
    }


}
